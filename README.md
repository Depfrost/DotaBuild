# Authors

- David GRONDIN (grondi_a)
- Nicolas TANG (tang_n)

# Project

DotaBuild is a website where you can find guides and stats about the game Dota 2 to improve your skills.
You can browse the website even if you're not registered.
But you cannot create or rate builds without an account.
