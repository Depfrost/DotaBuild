import { put, takeLatest, call } from 'redux-saga/effects'
import {
    GET_GENERAL_STATISTICS,
    GET_HEROES_STATISTICS_SUCCESS,
    GET_ITEMS_STATISTICS_SUCCESS,
    GET_LAST_GUIDES,
    GET_LAST_GUIDES_SUCCESS,
    GET_TOP_GUIDES,
    GET_TOP_GUIDES_SUCCESS
} from '../../constants/statistics/index'
import { getCallApi } from '../../utils/dotaAPI/index'
import { getCallApi as callBackendApi } from '../../utils/backendAPI/index'
import { errorNotification } from '../../utils/notification/index'

function* getHeroesStats() {
    try
    {
        const statsData = yield call(getCallApi, 'heroStats')
        const stats = statsData.data.map(elem => {
            const allVictories = elem["1_win"] + elem["2_win"] + elem["3_win"] + elem["4_win"] + elem["5_win"] + elem["6_win"] + elem["7_win"] + elem["8_win"]
            const allGames = elem["1_pick"] + elem["2_pick"] + elem["3_pick"] + elem["4_pick"] + elem["5_pick"] + elem["6_pick"] + elem["7_pick"] + elem["8_pick"]
            return {
                id: elem.id,
                hero_id: elem.hero_id,
                name: elem.name,
                localized_name: elem.localized_name,
                img: elem.img,
                icon: elem.icon,
                allGames,
                allVictories,
                allPercents: Math.round(allVictories * 100 / allGames),
                percents: [
                    Math.round(elem["1_win"] * 100 / elem["1_pick"]),
                    Math.round(elem["2_win"] * 100 / elem["2_pick"]),
                    Math.round(elem["3_win"] * 100 / elem["3_pick"]),
                    Math.round(elem["4_win"] * 100 / elem["4_pick"]),
                    Math.round(elem["5_win"] * 100 / elem["5_pick"]),
                    Math.round(elem["6_win"] * 100 / elem["6_pick"]),
                    Math.round(elem["7_win"] * 100 / elem["7_pick"]),
                    Math.round(elem["8_win"] * 100 / elem["8_pick"])
                ],
                wins: [
                    elem["1_win"],
                    elem["2_win"],
                    elem["3_win"],
                    elem["4_win"],
                    elem["5_win"],
                    elem["6_win"],
                    elem["7_win"],
                    elem["8_win"]
                ],
                picks: [
                    elem["1_pick"],
                    elem["2_pick"],
                    elem["3_pick"],
                    elem["4_pick"],
                    elem["5_pick"],
                    elem["6_pick"],
                    elem["7_pick"],
                    elem["8_pick"]
                ]
            }
        })
        yield put({type: GET_HEROES_STATISTICS_SUCCESS, stats: stats})
    }
    catch(e)
    {
        errorNotification('GET HEROES STATS', e.message)
        console.log(e)
    }
}

function* getItemsStats() {
    try
    {
        const itemsStats = yield call(getCallApi, 'scenarios/itemTimings')
        const data = itemsStats.data.map(elem => {
            return {
                ...elem,
                percent: Math.round(elem.wins * 100 / elem.games),
                time: Math.round(elem.time / 60)
            }
        })
        yield put({type: GET_ITEMS_STATISTICS_SUCCESS, stats: data})
    }
    catch(e)
    {
        errorNotification('GET ITEMS STATS', e.message)
        console.log(e)
    }
}

function* getTopGuides() {
    try
    {
        const topGuides = yield call(callBackendApi, 'guides/top')
        yield put({type: GET_TOP_GUIDES_SUCCESS, guides: topGuides.data})
    }
    catch(e)
    {
        errorNotification('GET TOP GUIDES', e.message)
        console.log(e)
    }
}

function* getLastGuides() {
    try
    {
        const lastGuides = yield call(callBackendApi, 'guides?limit=10')
        yield put({type: GET_LAST_GUIDES_SUCCESS, guides: lastGuides.data})
    }
    catch(e)
    {
        errorNotification('GET LAST GUIDES', e.message)
        console.log(e)
    }
}

export default function* statsSaga() {
    yield takeLatest(GET_GENERAL_STATISTICS, getHeroesStats),
    yield takeLatest(GET_GENERAL_STATISTICS, getItemsStats),
    yield takeLatest(GET_TOP_GUIDES, getTopGuides),
    yield takeLatest(GET_LAST_GUIDES, getLastGuides)
}