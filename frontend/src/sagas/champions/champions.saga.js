import { put, takeLatest, call, all } from 'redux-saga/effects'
import
{
    GET_ALL_HEROES,
    GET_ALL_HEROES_SUCCESS,
    GET_HEROE_PROFIL,
    GET_HEROE_PROFIL_SUCCESS
} from '../../constants/champions/index'

import { getCallApi } from '../../utils/dotaAPI/index'
import { errorNotification } from '../../utils/notification/index'

function* getAllHeroes() {
    try
    {
        //const heroesStats = yield call(getCallApi, 'heroStats')
        const heroes = yield call(getCallApi, 'heroes')
        yield put({ type: GET_ALL_HEROES_SUCCESS, champions: heroes.data })
    }
    catch (e)
    {
        errorNotification('ALL HEROES', e.message)
        console.log(e)
    }
}

function* getHeroeProfil(action) {
    try
    {
        const heroeMatchs = yield call(getCallApi, 'heroes/' + action.id + '/matches')
        const heroeMatchUps = yield call(getCallApi, 'heroes/' + action.id + '/matchups')
        const heroePlayers = yield call(getCallApi, 'heroes/' + action.id + '/players')
        
        // Get only 20
        const subHeroesMatch = heroeMatchs.data.slice(0, 20)

        // Get all player from match
        const heroesMatch = yield all(subHeroesMatch.map(elem => call(getCallApi, 'players/' + elem.account_id)))

        const lastMatchPlayer = subHeroesMatch.map(elem => {
            const player = heroesMatch.find(play => elem.account_id === play.data.profile.account_id)
            return {
                deaths: elem.deaths,
                assists: elem.assists,
                duration: Math.round(elem.duration / 60),
                kills: elem.kills,
                match_id: elem.match_id,
                league_id: elem.leagueid,
                ...player.data,
            }
        })

        const matchups = heroeMatchUps.data.map(elem => {
            return {
                ...elem,
                percent: Math.round(elem.wins * 100 / elem.games_played)
            }
        })
        
        const subPlayer = yield all(heroePlayers.data.slice(0, 10).map(elem => {return call(getCallApi, 'players/' + elem.account_id)}))
        const topplayers = subPlayer.map(elem => {
            const player = heroePlayers.data.find(play => play.account_id === elem.data.profile.account_id)
            return {
                ...elem.data,
                ...player,
                percent: Math.round(player.wins * 100 / player.games_played)
            }
        })
        
        const data = {
            heroe: action.id,
            matchs: lastMatchPlayer,
            matchups: matchups,
            players: topplayers
        }
        yield put({ type: GET_HEROE_PROFIL_SUCCESS, profil: data })
    }
    catch (e)
    {
        errorNotification('GET HEROE', e.message)
        console.log(e)
    }
}

export default function* championsSaga() {
    yield takeLatest(GET_ALL_HEROES, getAllHeroes)
    yield takeLatest(GET_HEROE_PROFIL, getHeroeProfil)
}