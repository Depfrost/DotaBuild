import { put, takeLatest } from 'redux-saga/effects'
import { errorNotification } from '../../utils/notification/index'
import { GET_ALL_ITEMS, GET_ALL_ITEMS_SUCCESS, GET_ITEM_PROFIL, GET_ITEM_PROFIL_SUCCESS } from '../../constants/items/index'
const items = require('../../utils/items.json')
const itemsData = require('../../utils/itemsProfil.json')


function* getAllItems() {
    try
    {
        /* WE SHOULD FIND AN API CALL */
        const itemsList = items.items.filter(elem => elem.recipe === 0 && elem.id < 1021).map(elem => {
            return {
                id: elem.id,
                name: elem.name,
                cost: elem.cost,
                localized_name: elem.localized_name,
                url_image: elem.url_image
            }
        })
        yield put({
            type: GET_ALL_ITEMS_SUCCESS,
            items: itemsList
        })
    }
    catch (e) 
    {
        errorNotification('GET ITEMS', e)
        console.log(e)
    }
}

function* getItemProfil(action) {
    try
    {
        for (const key in itemsData) {
            const obj = itemsData[key]
            if (parseInt(obj.ID) === action.id) {
                const requirements = obj.ItemRequirements !== undefined
                ? [...obj.ItemRequirements.map(elem => elem.split(";"))]
                : []
                const data = {
                    cost: obj.ItemCost,
                    tags: obj.ItemShopTags.split(";"),
                    name: obj.ItemAliases,
                    isUpdate: obj.ItemRequirements !== undefined,
                    requirements: requirements
                }
                yield put({type: GET_ITEM_PROFIL_SUCCESS, obj: data})
                break
            }
        }
    }
    catch(e)
    {
        errorNotification('GET ITEM', e.message)
        console.log(e)
    }
}

export default function* itemsSaga() {
    yield takeLatest(GET_ALL_ITEMS, getAllItems)
    yield takeLatest(GET_ITEM_PROFIL, getItemProfil)
}