import { put, takeLatest, call } from 'redux-saga/effects'
import {
    REGISTER_USER,
    CONNECTION_USER,
    CONNECTION_USER_SUCCESS,
    LOGOUT_USER,
    LOGOUT_USER_SUCCESS,
    GET_USER_INFORMATION,
    GET_USER_INFORMATION_SUCCESS,
    GET_USER_GUIDES_SUCCESS,
    DELETE_USER_GUIDE,
    DELETE_USER_GUIDE_SUCCESS
} from '../../constants/user/index'
import moment from 'moment'
import { postCallApi, getCallApi, deleteCallApi } from '../../utils/backendAPI/index'
import { errorNotification, successNotification } from '../../utils/notification/index'

function* registerUser(action) {
    try
    {
        yield call(postCallApi, 'user', action.user)
        successNotification('CREATE USER', 'Your account has been saved successfully !')
    }
    catch(e)
    {
        errorNotification('POST USER', e.message)
        console.log(e)
    }
}

function* connectionUser(action) {
    try
    {
        yield call(postCallApi, 'login', action.user)
        successNotification('CONNECTION', 'You are connected now ! Welcome !')
        yield put({type: CONNECTION_USER_SUCCESS})
    }
    catch(e)
    {
        errorNotification('CONNECTION', 'You connection failed, try again !')
        console.log(e)
    }
}

function* logoutUser() {
    try
    {
        yield call(getCallApi, 'logout')
        successNotification('DISCONNECT', 'You have disconnected your account successfully !')
        yield put({type: LOGOUT_USER_SUCCESS})
    }
    catch(e)
    {
        errorNotification('DISCONNECT', 'You failed to disconnect yourself !')
        console.log(e)
    }
}

function* getUserInformation() {
    try
    {
        const userProfil = yield call(getCallApi, 'user', true)
        yield put({type: GET_USER_INFORMATION_SUCCESS, user: userProfil.data})
    }
    catch(e)
    {
        errorNotification('GET USER INFORMATION', e.message)
        console.log(e)
    }
}

function* getUserGuides() {
    try
    {
        const userGuides = yield call(getCallApi, 'user/guides', true)
        const data = userGuides.data.map(elem => {
            return {
                ...elem,
                creation_date: moment(elem.creation_date).format('MM/DD/YYYY')
            }
        })
        yield put({type: GET_USER_GUIDES_SUCCESS, guides: data})
    }
    catch(e)
    {
        errorNotification('GET USER GUIDES', e.message)
        console.log(e)
    }
}

function* deleteUserGuide(action) {
    try
    {
        yield call(deleteCallApi, 'guide/' + action.id)
        yield put({type: DELETE_USER_GUIDE_SUCCESS, id: action.id})
        successNotification('DELETE', 'Your guide has been removed successfully')
    }
    catch(e)
    {
        errorNotification('DELETE GUIDE', 'Your guide cannot be removed !')
        console.log(e)
    }
}

export default function* userSaga() {
    yield takeLatest(REGISTER_USER, registerUser),
    yield takeLatest(CONNECTION_USER, connectionUser),
    yield takeLatest(LOGOUT_USER, logoutUser),
    yield takeLatest(GET_USER_INFORMATION, getUserInformation),
    yield takeLatest(GET_USER_INFORMATION, getUserGuides),
    yield takeLatest(DELETE_USER_GUIDE, deleteUserGuide)
}