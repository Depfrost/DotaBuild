import { fork } from 'redux-saga/effects'
/* IMPORT FULL SAGAS */
import championsSaga from './champions/champions.saga'
import guidesSaga from './guides/guides.saga'
import itemsSaga from './items/items.saga'
import statsSaga from './statistics/statistics.saga'
import userSaga from './user/user.saga'

const sagas = [
    /* ADD EVERY SAGA HERE */
    championsSaga,
    guidesSaga,
    itemsSaga,
    statsSaga,
    userSaga
]

function* rootSaga() {
    yield sagas.map(saga => fork(saga))
}

export default rootSaga