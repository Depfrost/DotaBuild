import { put, takeLatest, call } from 'redux-saga/effects'
import { errorNotification, successNotification } from '../../utils/notification'
import { postCallApi, getCallApi } from '../../utils/backendAPI/index'
import {GET_ALL_GUIDES, GET_ALL_GUIDES_SUCCESS, GET_GUIDE_PAGE, GET_GUIDE_PAGE_SUCCESS, CREATE_GUIDE, RATE_GUIDE} from "../../constants/guides";

function* createGuide(action) {
    try
    {
        const formatedData = {
            hero_id: action.guide.hero_id,
            title: action.guide.title,
            content: action.guide.description,
            necessary: action.guide.necessary === undefined ? [] : action.guide.necessary,
            situational: action.guide.situational === undefined ? [] : action.guide.situational,
            good: action.guide.good === undefined ? [] : action.guide.good,
        }
        yield call(postCallApi, 'guides', formatedData, true)
        successNotification('CREATE GUIDE', 'Your guide has been saved successfully !')
    }
    catch(e)
    {
        errorNotification('CREATE GUIDE', 'Your guide cannot be saved !')
        console.log(e)
    }
}

function* rateGuide(action) {
    try
    {
        const formatedData = {
            guide_id: action.guide_id,
            note: action.note,
        }
        yield call(postCallApi, 'notes', formatedData, true)
        successNotification('RATE GUIDE', 'Your note has been taken into account')
        yield put({ type: GET_GUIDE_PAGE, id: action.guide_id})
    }
    catch(e)
    {
        errorNotification('RATE GUIDE', 'Your note cannot be saved !')
        console.log(e)
    }
}

function* getAllGuides() {
    try
    {
        const guides = yield call(getCallApi, 'guides')
        guides.data.map(elem => {
            let date = new Date(elem.creation_date)
            elem.creation_date = date.toLocaleDateString("en-US")
            return elem
        })
        yield put({ type: GET_ALL_GUIDES_SUCCESS, guides: guides.data })
    }
    catch (e)
    {
        errorNotification('ALL GUIDES', e.message)
        console.log(e)
    }
}

function* getGuidePage(action) {
    try
    {
        const guidePage = yield call(getCallApi, 'guides')
        let guide = guidePage.data.find(elem => elem._id === action.id)
        let date = new Date(guide.creation_date)
        guide.creation_date = date.toLocaleDateString("en-US")

        yield put({ type: GET_GUIDE_PAGE_SUCCESS, guide: guide })
    }
    catch (e)
    {
        errorNotification('GET GUIDE', e.message)
        console.log(e)
    }
}

export default function* guidesSaga() {
    /* PUT ALL YIELDS */
    yield takeLatest(CREATE_GUIDE, createGuide)
    yield takeLatest(RATE_GUIDE, rateGuide)
    yield takeLatest(GET_ALL_GUIDES, getAllGuides)
    yield takeLatest(GET_GUIDE_PAGE, getGuidePage)
}