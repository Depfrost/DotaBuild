import React from 'react'
import Menu from '../../components/menu/menu'
import { connect } from 'react-redux'

class HeaderBar extends React.Component {
    render() {
        return (
            <div>
                <Menu user={this.props.user} />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.user
})

export default connect(mapStateToProps)(HeaderBar)