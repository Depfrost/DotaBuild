import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getAllItems, getItemProfil } from '../../actions/items/index'
import { List, Row, Col, Modal, Input } from 'antd'
import ItemProfil from '../../components/items/itemProfil'

class ItemsList extends React.Component {
    constructor(props) {
        super(props)
        this.props.actions.getAllItems()
        this.state = {
            modalVisible: false,
            selectedItem: {},
            filter: ""
        }
    }

    handleSelectedItem = (record) => {
        this.setState({
            modalVisible: true,
            selectedItem: record
        })
        this.props.actions.getItemProfil(record.id)
    }

    render() {
        return (
            <div>
                <h1 style={{textAlign: 'center'}}>Items List</h1>
                <Row>
                    <Col span={1} />
                    <Col span={22}>
                        <Row>
                            <Input placeholder="Look for a specific item" onChange={(e) => this.setState({filter: e.target.value})}/>
                        </Row>
                        <List
                            itemLayout="horizontal"
                            dataSource={this.props.items.filter(elem => elem.localized_name.toLowerCase().match(this.state.filter))}
                            loading={this.props.items.length === 0}
                            renderItem={item => {
                                const url = item.url_image
                                return (
                                    <a>
                                        <List.Item onClick={() => this.handleSelectedItem(item)}>
                                            <List.Item.Meta
                                                avatar={<img alt={item.id} src={url}/>}
                                                title={item.localized_name}
                                                description={"cost : " + item.cost}
                                            />
                                        </List.Item>
                                    </a>
                                )
                            }}
                        />
                        <Modal
                            title={this.state.selectedItem.localized_name}
                            visible={this.state.modalVisible}
                            onOk={() => this.setState({modalVisible: false})}
                            onCancel={() => this.setState({modalVisible: false})}
                        >
                            <ItemProfil item={this.props.item} />
                        </Modal>
                    </Col>
                    <Col span={1} />
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    items: state.items,
    item: state.item
})

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            getAllItems: bindActionCreators(getAllItems, dispatch),
            getItemProfil: bindActionCreators(getItemProfil, dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList)