import React from 'react'
import { connect } from 'react-redux'
import { Form, Input, Button } from 'antd'
import { bindActionCreators } from 'redux'
import { registerUser } from '../../actions/user/index'

class InscriptionForm extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.actions.registerUser({
                    email: values.email,
                    password: values.password,
                    username: values.username
                })
                this.props.history.push('/connexion')
            }
        });
    }

    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
    }

    render() {
        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
        }
        const tailFormItemLayout = {
            wrapperCol: {
              xs: {
                span: 24,
                offset: 0,
              },
              sm: {
                span: 16,
                offset: 8,
              },
            },
        }
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <h1 style={{textAlign: 'center'}}>Inscription</h1>
                <Form onSubmit={this.handleSubmit}
                >
                    <Form.Item label="Username" {...formItemLayout}>
                    {
                        getFieldDecorator('username', {
                            rules: [{
                                required: true, message: 'Please input your Username !',
                            }],
                        })(
                            <Input placeholder="Username"/>
                        )
                    }
                    </Form.Item>
                    <Form.Item label="Email" {...formItemLayout}>
                    {
                        getFieldDecorator('email', {
                            rules: [{
                                type: 'email', message: 'The input is not valid E-mail !',
                            }, {
                                required: true, message: 'Please input your E-mail !',
                            }],
                        })(
                            <Input type="email" placeholder="Email"/>
                        )
                    }
                    </Form.Item>
                    <Form.Item label="Password" {...formItemLayout}>
                    {
                        getFieldDecorator('password', {
                            rules: [{
                                required: true, message: 'Please input password !',
                            }]
                        })(
                            <Input type="password" placeholder="Password"/>
                        )
                    }
                    </Form.Item>
                    <Form.Item label="Repeat Password" {...formItemLayout}>
                    {
                        getFieldDecorator('repeat password', {
                            rules: [{
                                required: true, message: 'The password are not the same !',
                            }, {
                                validator: this.compareToFirstPassword
                            }],
                        })(
                            <Input type="password" placeholder="Repeat Password" />
                        )
                    }
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">Register</Button>
                    </Form.Item>
                </Form>
            </div>
        )
    }
}

const WrappedInscription = Form.create()(InscriptionForm);

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            registerUser: bindActionCreators(registerUser, dispatch)
        }
    }
}

export default connect(null, mapDispatchToProps)(WrappedInscription)