import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getUserInformation, deleteUserGuide } from '../../actions/user/index'
import { Table, Row, Col, Button } from 'antd'
import ErrorPage from '../../components/utils/error'

class UserProfil extends React.Component {
    constructor(props) {
        super(props)
        this.props.actions.getUserInformation()
        this.columns = [
            {
                title: "Your guides",
                children: [
                    {
                        title: "Name",
                        dataIndex: 'title'
                    }, {
                        title: "Heroe",
                        dataIndex: "heroe"
                    }, {
                        title: 'Description',
                        dataIndex: 'content'
                    }, {
                        title: "Mark / 10",
                        dataIndex: 'score',
                    }, {
                        title: "Number of mark",
                        dataIndex: "nb_notes"
                    }, {
                        title: "Creation Date",
                        dataIndex: "date"
                    }, {
                        title: "Action",
                        dataIndex: "action",
                        render: (_, record) => {
                            return <Button type="danger" onClick={(e) => this.props.actions.deleteUserGuide(record.key)}>Delete</Button>
                        }
                    }
                ]
            }
        ]
    }

    render() {
        return this.props.user.isConnected ? (
            <div>
                <h1 style={{textAlign: 'center'}}>Your Profile</h1>
                <Row>
                    <Col span={6}>
                        <h3>Username : {this.props.user.username}</h3>
                        <h3>Email : {this.props.user.email}</h3>
                    </Col>
                    <Col span={1}/>
                    <Col span={17}>
                        <Table
                            columns={this.columns}
                            dataSource={this.props.user.guides.map(elem => {
                                const heroe = this.props.heroes.find(hero => hero.hero_id === elem.hero_id).localized_name
                                const data = {
                                    key: elem._id,
                                    score: elem.score["$numberDecimal"],
                                    heroe: heroe,
                                    title: elem.title,
                                    content: elem.content,
                                    nb_notes: elem.nb_notes,
                                    date: elem.creation_date
                                }
                                return data
                            })}
                        />
                    </Col>
                </Row>
            </div>
        )
        : <ErrorPage />
    }
}

const mapStateToProps = states => ({
    user: states.user,
    heroes: states.stats.heroes
})

const mapDispatchToProps = dispatch => ({
    actions: {
        getUserInformation: bindActionCreators(getUserInformation, dispatch),
        deleteUserGuide: bindActionCreators(deleteUserGuide, dispatch)
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(UserProfil)