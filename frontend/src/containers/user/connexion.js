import React from 'react'
import { connect } from 'react-redux'
import { Form, Input, Button } from 'antd'
import { connectionUser } from '../../actions/user/index'
import { bindActionCreators } from 'redux'

class ConnexionForm extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.actions.connectionUser(values);
                this.props.history.push('/')
            }
        });
    }

    render() {
        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
        }
        const tailFormItemLayout = {
            wrapperCol: {
              xs: {
                span: 24,
                offset: 0,
              },
              sm: {
                span: 16,
                offset: 8,
              },
            },
        }
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <h1 style={{textAlign: 'center'}}>Connexion</h1>
                <Form onSubmit={this.handleSubmit}
                >
                    <Form.Item label="Username" {...formItemLayout}>
                    {
                        getFieldDecorator('username', {
                            rules: [{
                                required: true, message: 'Please input your Username !',
                            }],
                        })(
                            <Input placeholder="Username"/>
                        )
                    }
                    </Form.Item>
                    <Form.Item label="Password" {...formItemLayout}>
                    {
                        getFieldDecorator('password', {
                            rules: [{
                                required: true, message: 'Please input password !',
                            }]
                        })(
                            <Input type="password" placeholder="Password"/>
                        )
                    }
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">Sign in</Button>
                    </Form.Item>
                </Form>
            </div>
        )
    }
}

const WrappedConnexion = Form.create()(ConnexionForm);

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            connectionUser: bindActionCreators(connectionUser, dispatch)
        }
    }
}

export default connect(null, mapDispatchToProps)(WrappedConnexion)