import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getGeneralStats } from '../../actions/statistics/index'
import { Row, Tabs } from 'antd'
import HeroesTab from '../../components/statistics/heroesTab'
import ItemsTab from '../../components/statistics/itemsTab'

const TabPane = Tabs.TabPane

class StaticticsList extends React.Component {
    constructor(props) {
        super(props)
        this.props.actions.getGeneralStats()
        this.columns = [
            {
                title: 'Name',
                dataIndex: 'name'
            }, {
                title: 'Number of Games',
                dataIndex: 'games',
                sorter: (b, a) => a.games - b.games
            }, {
                title: 'Wins Rate',
                dataIndex: 'percent',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent - b.percent
            }, {
                title: '1 pick',
                dataIndex: 'percent1',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent1 - b.percent1
            }, {
                title: '2 pick',
                dataIndex: 'percent2',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent2 - b.percent2
            }, {
                title: '3 pick',
                dataIndex: 'percent3',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent3 - b.percent3
            }, {
                title: '4 pick',
                dataIndex: 'percent4',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent4 - b.percent4
            }, {
                title: '5 pick',
                dataIndex: 'percent5',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent5 - b.percent5
            }, {
                title: '6 pick',
                dataIndex: 'percent6',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent6 - b.percent6
            }, {
                title: '7 pick',
                dataIndex: 'percent7',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent7 - b.percent7
            }, {
                title: '8 pick',
                dataIndex: 'percent8',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent8 - b.percent8
            }
        ]        
    }

    renderColumns = (val) => {
        if (val > 50) {
            return <font color="green">{val} %</font>
        }
        if (val < 50) {
            return <font color="red">{val} %</font>
        }
        return <font>{val} %</font>
    }

    render() {
        return (
            <div>
                <h1 style={{textAlign: 'center'}}>General Statistics</h1>
                <Row>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Heroes" key="1">
                            <HeroesTab stats={this.props.stats.heroes.map(elem => {
                                return {
                                    key: elem.id,
                                    name: elem.localized_name,
                                    games: elem.allGames,
                                    percent: elem.allPercents,
                                    percent1: elem.percents[0],
                                    percent2: elem.percents[1],
                                    percent3: elem.percents[2],
                                    percent4: elem.percents[3],
                                    percent5: elem.percents[4],
                                    percent6: elem.percents[5],
                                    percent7: elem.percents[6],
                                    percent8: elem.percents[7]
                                }
                            })} />
                        </TabPane>
                        <TabPane tab="Items" key="2">
                            <ItemsTab stats={this.props.stats.items.map((elem, id) => {
                                return  {
                                    ...elem,
                                    key: id,
                                    heroe: this.props.stats.heroes.find(hero => hero.hero_id === elem.hero_id).localized_name
                                }
                            })} />
                        </TabPane>
                    </Tabs>
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    stats: state.stats
})

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            getGeneralStats: bindActionCreators(getGeneralStats, dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StaticticsList)