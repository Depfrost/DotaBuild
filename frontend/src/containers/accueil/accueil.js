import React from 'react'
import { connect } from 'react-redux'
import { Row, Col, Card } from 'antd'
import { bindActionCreators } from 'redux'
import { getGeneralStats, getLastGuides, getTopGuides } from '../../actions/statistics/index'

const settings = require('../../settings.json')

class AccueilPage extends React.Component {
    constructor(props) {
        super(props)
        this.props.actions.getLastGuides()
        this.props.actions.getTopGuides()
        this.props.actions.getGeneralStats()
    }

    render() {
        return (
            <div>
                <h1 style={{textAlign: 'center'}}>Actualities</h1>
                <Row>
                    <Col span={1} />
                    <Col span={10}>
                        <Card
                            title="Lastest Guides !"
                            extra={<a onClick={() => this.props.history.push('/guides')}>More</a>}
                        >
                        {
                            this.props.lastGuides.map((elem, id) => {
                                const url = this.props.heroes.find(hero => hero.hero_id === elem.hero_id)
                                return (
                                    <div key={id}>
                                        <p>
                                            <img alt={elem.id} src={url === undefined ? null : (settings.dota_app + url.icon)} />
                                            {elem.title}
                                        </p>
                                    </div>
                                )
                            })
                        }
                        </Card>
                    </Col>
                    <Col span={2} />
                    <Col span={10}>
                        <Card
                            title="Top Guides !"
                            extra={<a onClick={() => this.props.history.push('/guides')}>More</a>}
                        >
                        {
                            this.props.topGuides.map((elem, id) => {
                                const url = this.props.heroes.find(hero => hero.hero_id === elem.hero_id)
                                return (
                                    <div key={id}>
                                        <p>
                                            <img alt={elem.id} src={url === undefined ? null : (settings.dota_app + url.icon)} />
                                            {elem.title}
                                        </p>
                                    </div>
                                )
                            })
                        }
                        </Card>
                    </Col>
                    <Col span={1} />
                </Row>
                <br/>
                <Row>
                    <Col span={1} />
                    <Col span={6}>
                        <Card
                            title="Top Heroes !"
                            extra={<a onClick={() => this.props.history.push('/statistics')}>More</a>}
                        >
                        {
                            this.props.topHeroes.map(elem => {
                                return (
                                    <div key={elem.id}>
                                        <p>
                                            <img alt={elem.id} src={settings.dota_app + elem.icon} />
                                            {elem.localized_name}
                                        </p>
                                    </div>
                                )
                            })
                        }
                        </Card>
                    </Col>
                    <Col span={1} />
                    <Col span={6}>
                        <Card
                            title="Most Played Heroes !"
                            extra={<a onClick={() => this.props.history.push('/statistics')}>More</a>}
                        >
                        {
                            this.props.mostHeroes.map(elem => {
                                return (
                                    <div key={elem.id}>
                                        <p>
                                            <img alt={elem.id} src={settings.dota_app + elem.icon} />
                                            {elem.localized_name}
                                        </p>
                                    </div>
                                )
                            })
                        }
                        </Card>
                    </Col>
                    <Col span={1} />
                    <Col span={6}>
                        <Card
                            title="Most Efficient Items !"
                            extra={<a onClick={() => this.props.history.push('/statistics')}>More</a>}
                        >
                        {
                            this.props.mostItems.map((elem, id) => {
                                const url = this.props.heroes.find(hero => hero.hero_id === elem.hero_id)
                                return (
                                    <div key={id}>
                                        <p>
                                            <img alt={id} src={url === undefined ? null : (settings.dota_app + url.icon)} />
                                            <font color="green">{" <- " + elem.item}</font>
                                        </p>
                                    </div>
                                )
                            })
                        }
                        </Card>
                    </Col>
                    <Col span={1} />
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    topHeroes: state.stats.heroes.sort((a, b) => b.allPercents - a.allPercents).slice(0, 10),
    mostHeroes: state.stats.heroes.sort((a, b) => b.allGames - a.allGames).slice(0, 10),
    mostItems: state.stats.items.filter(elem => elem.games > 1000).sort((a, b) => b.percent - a.percent).slice(0, 10),
    heroes: state.stats.heroes,
    topGuides: state.stats.topGuides,
    lastGuides: state.stats.lastGuides
})

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            getGeneralStats: bindActionCreators(getGeneralStats, dispatch),
            getTopGuides: bindActionCreators(getTopGuides, dispatch),
            getLastGuides: bindActionCreators(getLastGuides, dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccueilPage)