import React from 'react'
import { connect } from 'react-redux'
import { List, Avatar, Row, Col, Tabs } from 'antd'
import { getAllGuides } from '../../actions/guides/index'
import TabsList from '../../components/champions/index'
import { bindActionCreators } from 'redux';

const settings = require('../../settings.json')
const TabPane = Tabs.TabPane
const { LastMatchsTab, MatchupsTab, TopPlayerTab, LastGuidesTab } = TabsList

class ChampionProfil extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: 0,
            name: "",
            roles: [],
            localized_name: "",
            abilities: []
        }
        this.props.actions.getAllGuides()
    }

    componentWillReceiveProps(nextprops) {
        const hero = nextprops.champions.find(elem => elem.id === nextprops.champion.heroe)
        if (hero !== undefined) {
            this.setState({
                id: hero.id,
                name: hero.name.substring(14),
                localized_name: hero.localized_name,
                roles: hero.roles
            })
        }
    }

    render() {
        return (
            <div>
                <br />
                <Row>
                    <Col span={1} />
                    <Col span={6}>
                        <Avatar
                            style={{width: "100%", height: "100%"}}
                            src={this.state.name !== ""
                                ? settings.dota_cdn + 'apps/dota2/images/heroes/' + this.state.name + '_vert.jpg'
                                : null
                            }
                        />
                    </Col>
                    <Col span={2} />
                    <Col span={14}>
                        <Row>
                            <h3>Name : {this.state.localized_name}</h3>
                        </Row>
                        <Row>
                            <h3>Roles :</h3>
                            <List
                                size="small"
                                dataSource={this.state.roles}
                                renderItem={item => (<List.Item>{item}</List.Item>)}
                            />               
                        </Row>
                    </Col>
                    <Col span={1} />
                </Row>
                <Row>
                    <Col span={1} />
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Last Matchs" key="1">
                            <LastMatchsTab matchs={this.props.champion.matchs} />
                        </TabPane>
                        <TabPane tab="Matchups" key="2">
                            <MatchupsTab champions={this.props.champions} matchups={this.props.champion.matchups}/>
                        </TabPane>
                        <TabPane tab="Top Players" key="3">
                            <TopPlayerTab players={this.props.champion.players} />
                        </TabPane>
                        <TabPane tab="Last Guides" key="4">
                            <LastGuidesTab hero_id={this.state.id} guides={this.props.guides} img={settings.dota_cdn + 'apps/dota2/images/heroes/' + this.state.name + '_vert.jpg'} />
                        </TabPane>
                    </Tabs>
                    <Col span={1} />
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    champion: state.champion,
    champions: state.champions,
    guides: state.guides
})

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            getAllGuides: bindActionCreators(getAllGuides, dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChampionProfil)
