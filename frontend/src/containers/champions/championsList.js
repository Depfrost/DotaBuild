import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getAllChampions, getChampionProfil, resetChampionProfil } from '../../actions/champions/index'
import { withRouter } from 'react-router-dom'
import { List, Row, Col, Input } from 'antd'

const settings = require('../../settings.json')

class ChampionsList extends React.Component {
    constructor(props) {
        super(props)
        this.props.actions.getAllHeroes()
        this.state = {
            filter: ""
        }
    }
    
    render() {
        return (
            <div>
                <h1 style={{textAlign: 'center'}}>Heroes List</h1>
                <Row>
                    <Col span={1} />
                    <Col span={22}>
                        <Row>
                            <Input placeholder="Look for a specific hero" onChange={(e) => this.setState({filter: e.target.value})}/>
                        </Row>
                        <List
                            itemLayout="horizontal"
                            loading={this.props.champions.length === 0}
                            dataSource={this.props.champions.filter(elem => elem.localized_name.toLowerCase().match(this.state.filter))}
                            renderItem={item => {
                                const url = settings.dota_cdn + 'apps/dota2/images/heroes/' + item.name.substring(14) + '_sb.png'
                                return (
                                    <a>
                                    <List.Item onClick={() => {
                                        this.props.actions.resetChampionProfil()
                                        this.props.actions.getChampionProfil(item.id)
                                        this.props.history.push('/championProfil')
                                    }} >
                                        <List.Item.Meta
                                            avatar={<img alt={item.name} src={url}/>}
                                            title={item.localized_name}
                                            description={item.attack_type}
                                        />
                                    </List.Item>
                                    </a>
                                )
                            }}
                        />
                    </Col>
                    <Col span={1} />
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    champions: state.champions
})

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            getAllHeroes: bindActionCreators(getAllChampions, dispatch),
            getChampionProfil: bindActionCreators(getChampionProfil, dispatch),
            resetChampionProfil: bindActionCreators(resetChampionProfil, dispatch)
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ChampionsList))