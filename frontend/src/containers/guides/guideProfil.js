import React from 'react'
import { connect } from 'react-redux';
import { Avatar, Row, Col, Icon, Tag } from 'antd'
import {bindActionCreators} from 'redux';
import {getGeneralStats} from "../../actions/statistics";
import {rateGuide} from "../../actions/guides";

const settings = require('../../settings.json')

class GuideProfil extends React.Component {
    constructor(props) {
        super(props)
        this.props.actions.getGeneralStats()
        this.state = {
            guide_id: "",
            user_id: "",
            username: "",
            hero_id: 0,
            title: "",
            necessary: [],
            good: [],
            situational: [],
            content: "",
            score: 0,
            nb_notes: 0,
            creation_date: "",
            img: "",
        }
    }

    componentWillReceiveProps(nextprops) {
        const guide = nextprops.guides.find(elem => elem._id === nextprops.guide._id)
        if (guide !== undefined) {
            let hero = this.props.stats.heroes.find(elem => elem.hero_id === guide.hero_id)
            this.setState({
                guide_id: guide._id,
                user_id: guide.id,
                username: guide.username,
                hero_id: guide.hero_id,
                title: guide.title,
                necessary: guide.necessary,
                good: guide.good,
                situational: guide.situational,
                content: guide.content,
                score: guide.score,
                nb_notes: guide.nb_notes,
                creation_date: guide.creation_date,
                img: hero.img,
            })
        }
    }

    upVote = (id) => {
        //this.props.actions.rateGuide(id, 10)
    }
    downVote = (id) => {
        //this.props.actions.rateGuide(id, 0)
    }

    render() {
        return (
            <div>
                <Row>
                <br/>

                <Col span={1} />
                <Col span={22}>
                    <Row>
                        <Col span={6}>
                            <Avatar
                                style={{width: "100%", height: "100%"}}
                                src={settings.dota_app + this.state.img}
                            />
                        </Col>
                        <Col span={10}>
                            <h1 style={{textAlign: 'center'}}>{this.state.title}</h1>
                        </Col>
                        <Col span={6}>
                            <h3 style={{textAlign: 'center'}}>Score: {this.state.score["$numberDecimal"]}</h3>
                            <h3 style={{textAlign: 'center'}}>Votes: {this.state.nb_notes}</h3>
                            <Row>
                                <Icon style={{Align: 'right'}} type="like" style={{ fontSize: 24, color: '#88CC00' }} onClick={() => this.props.actions.rateGuide(this.state.guide_id, 10)}/>
                                <Icon style={{Align: 'right'}} type="dislike" style={{ fontSize: 24, color: '#b73d00' }} onClick={() => this.props.actions.rateGuide(this.state.guide_id, 0)}/>
                            </Row>
                        </Col>
                    </Row>

                    <br/>

                    <Row>
                        <h3>Core items:</h3>
                        {this.state.necessary.map(item => <Tag key={item} closable={false}>{item}</Tag>)}
                        <br/>
                    </Row>

                    <Row>
                        <h3>Good items:</h3>
                        {this.state.good.map(item => <Tag key={item} closable={false}>{item}</Tag>)}
                        <br/>
                    </Row>

                    <Row>
                        <h3>Situational items:</h3>
                        {this.state.situational.map(item => <Tag key={item} closable={false}>{item}</Tag>)}
                        <br/>
                    </Row>

                    <br/>

                    <Row>
                        <h2>Description:</h2>
                        {this.state.content}
                    </Row>

                    <br/>

                    <Row>
                        <h3 style={{textAlign: 'right'}}>Guide written by: {this.state.username}</h3>
                    </Row>

                    <h3 style={{textAlign: 'right'}}>Created the: {this.state.creation_date}</h3>
                </Col>

                <Col span={1} />
                </Row>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    guide: state.guide,
    guides: state.guides,
    stats: state.stats
})

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            getGeneralStats: bindActionCreators(getGeneralStats, dispatch),
            rateGuide: bindActionCreators(rateGuide, dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GuideProfil)