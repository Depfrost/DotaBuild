import React from 'react'
import { connect } from 'react-redux';
import { Form, Input, AutoComplete, Button, Select } from 'antd'
import { createGuide } from '../../actions/guides/index'
import ErrorPage from '../../components/utils/error'
import { bindActionCreators } from 'redux';
import { errorNotification } from '../../utils/notification'

class GuideCreate extends React.Component {
    constructor(props) {
        super(props)
        this.formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
        }
        this.tailFormItemLayout = {
            wrapperCol: {
              xs: {
                span: 24,
                offset: 0,
              },
              sm: {
                span: 16,
                offset: 8,
              },
            },
        }
    }
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const heroe = this.props.heroes.find(elem => elem.localized_name === values.heroe)
                if (heroe !== undefined) {
                    this.props.actions.createGuide({...values, hero_id: heroe.hero_id})
                    this.props.history.push('/guides')
                } else {
                    errorNotification('HEROE', "Please choose a real that exist")
                }
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const dataSource = this.props.items.map(elem => elem.item)
        return this.props.user.isConnected ? (
            <div>
                <h1 style={{textAlign: 'center'}}>Guide Creation</h1>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Item label="Title" {...this.formItemLayout}>
                    {
                        getFieldDecorator('title', {
                            rules: [{
                                required: true, message: 'Please input a title for your guide',
                            }],
                        })(
                            <Input placeholder="Title"/>
                        )
                    }
                    </Form.Item>
                    <Form.Item label="Heroe" {...this.formItemLayout}>
                    {
                        getFieldDecorator('heroe', {
                            rules: [{
                                required: true, message: 'Please choose a hero for your guide',
                            }],
                        })(
                            <AutoComplete
                                placeholder="Choose your hero"
                                dataSource={this.props.heroes.map(elem => {
                                    return elem.localized_name
                                })}
                                filterOption={(inputValue, option) => option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1}
                            />
                        )
                    }
                    </Form.Item>
                    <Form.Item label="Description" {...this.formItemLayout}>
                    {
                        getFieldDecorator('description', {
                            rules: [{
                                required: true, message: 'Please write a short description',
                            }]  
                        }) (
                            <Input.TextArea rows={5} placeholder="Description..." />
                        )
                    }
                    </Form.Item>
                    <Form.Item label="Necessary Items" {...this.formItemLayout}>
                    {
                        getFieldDecorator('necessary', {}) (
                            <Select mode="multiple" placeholder="Please select">
                            {
                                
                                dataSource.map(elem => <Select.Option key={elem}>{elem}</Select.Option>)
                            }
                            </Select>
                        )
                    }
                    </Form.Item>
                    <Form.Item label="Good Items" {...this.formItemLayout}>
                    {
                        getFieldDecorator('good', {}) (
                            <Select mode="multiple" placeholder="Please select">
                            {
                                dataSource.map(elem => <Select.Option key={elem}>{elem}</Select.Option>)
                            }
                            </Select>
                        )
                    }
                    </Form.Item>
                    <Form.Item label="Situationals Items" {...this.formItemLayout}>
                    {
                        getFieldDecorator('situationals', {}) (
                            <Select mode="multiple" placeholder="Please select">
                            {
                                dataSource.map(elem => <Select.Option key={elem}>{elem}</Select.Option>)
                            }
                            </Select>
                        )
                    }
                    </Form.Item>
                    <Form.Item {...this.tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">Create Guide</Button>
                    </Form.Item>
                </Form>
            </div>
        )
        : <ErrorPage />
    }
}

const WrappedGuideCreation = Form.create()(GuideCreate);

const mapStateToProps = state => {
    const items = []
    for (const iterator of state.stats.items) {
        if (items.find(elem => elem.item === iterator.item) === undefined) {
            items.push(iterator)
        }
    }
    return {
        heroes: state.stats.heroes,
        items: items,
        user: state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            createGuide: bindActionCreators(createGuide, dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WrappedGuideCreation)