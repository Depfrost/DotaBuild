import React from 'react'
import { List, Button } from 'antd'
import {getAllGuides, getGuidePage} from "../../actions/guides";
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";

class GuideList extends React.Component {
    constructor(props) {
        super(props)
        this.props.actions.getAllGuides()
    }

    render() {
        const data = this.props.guides
        return (
            <div>
                <h1 style={{textAlign: 'center'}}>Guides List</h1>
                {
                    this.props.user.isConnected
                        ? <Button type="primary" onClick={
                            () => this.props.history.push('/guideCreate')
                        }>Create a Guide</Button>
                        : null
                }
                <List
                    itemLayout="vertical"
                    size="large"
                    pagination={{
                        pageSize: 10
                    }}
                    dataSource={data}
                    renderItem={item => (


                        <List.Item key={item._id} onClick={() => {
                            this.props.actions.getGuidesPage(item._id)
                            this.props.history.push('/guideProfil')
                        }}>
                            <List.Item.Meta
                                title={item.title}
                                description={item.creation_date}
                            />
                        </List.Item>
                    )}
                />
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    guides: state.guides,
    user: state.user
})

const mapDispatchToProps = dispatch => {
    return {
        actions: {
            getAllGuides: bindActionCreators(getAllGuides, dispatch),
            getGuidesPage: bindActionCreators(getGuidePage, dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GuideList)