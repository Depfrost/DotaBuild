import React from 'react'
import 'antd/dist/antd.css'
import Header from './header/headerBar'
import { Layout, Col } from 'antd'
import { Route } from 'react-router-dom'
import Champions from './champions/championsList'
import Guides from './guides/guideList'
import GuideProfil from './guides/guideProfil'
import Items from './items/itemsList'
import Accueil from './accueil/accueil'
import ChampionProfil from './champions/championProfil'
import StatisticsList from './statistics/statisticsList'
import Inscription from './user/inscription'
import Connexion from './user/connexion'
import UserProfil from './user/userProfil'
import GuideCreate from './guides/guideCreate'

class Application extends React.Component {
    render() {
        return(
            <div>
                <Header />
                <Layout.Content>
                    <Col span={1} />
                    <Col span={22}>
                        <Route exact path="/" component={Accueil}></Route>
                        <Route path="/champions" component={Champions}></Route>
                        <Route path="/guides" component={Guides}></Route>
                        <Route path="/guideProfil" component={GuideProfil}></Route>
                        <Route path="/items" component={Items}></Route>
                        <Route path="/statistics" component={StatisticsList}></Route>
                        <Route path="/championProfil" component={ChampionProfil}></Route>
                        <Route path="/inscription" component={Inscription}></Route>
                        <Route path="/connexion" component={Connexion}></Route>
                        <Route path="/profil" component={UserProfil}></Route>
                        <Route path="/guideCreate" component={GuideCreate}></Route>
                        <br/>
                        <h3 style={{textAlign: 'center'}}>DotaBuild ©2018 Created by Nicolas TANG / David GRONDIN</h3>
                    </Col>
                    <Col span={1}/>
                </Layout.Content>
            </div>
        )
    }
}

export default Application