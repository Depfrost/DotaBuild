/* SAGA CONSTANTS */
export const REGISTER_USER = 'REGISTER_USER'
export const CONNECTION_USER = 'CONNECTION_USER'
export const LOGOUT_USER = 'LOGOUT_USER'
export const GET_USER_INFORMATION = 'GET_USER_INFORMATION'
export const DELETE_USER_GUIDE = 'DELETE_USER_GUIDE'

/* REDUCER CONSTANTS */
export const CONNECTION_USER_SUCCESS = 'CONNECTION_USER_SUCCESS'
export const LOGOUT_USER_SUCCESS = 'LOGOUT_USER_SUCCESS'
export const GET_USER_INFORMATION_SUCCESS = 'GET_USER_INFORMATION_SUCCESS'
export const GET_USER_GUIDES_SUCCESS = 'GET_USER_GUIDES_SUCCESS'
export const DELETE_USER_GUIDE_SUCCESS = 'DELETE_USER_GUIDE_SUCCESS'