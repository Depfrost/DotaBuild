import { GET_ALL_ITEMS, GET_ITEM_PROFIL } from '../../constants/items/index'

export const getAllItems = () => ({type: GET_ALL_ITEMS})
export const getItemProfil = (id) => ({type: GET_ITEM_PROFIL, id: id})