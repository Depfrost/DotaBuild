import { REGISTER_USER, CONNECTION_USER, LOGOUT_USER, GET_USER_INFORMATION, DELETE_USER_GUIDE } from '../../constants/user/index'
import store from '../../store'

export const registerUser = (values) => ({type: REGISTER_USER, user: values})
export const connectionUser = (values) => ({type: CONNECTION_USER, user: values})
export const logOut = () => store.dispatch({type: LOGOUT_USER})
export const getUserInformation = () => ({type: GET_USER_INFORMATION})
export const deleteUserGuide = (id) => ({type: DELETE_USER_GUIDE, id:id})