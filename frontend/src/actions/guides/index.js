import { CREATE_GUIDE } from '../../constants/guides/index'
import * as constants from '../../constants/guides/index'

export const createGuide = (values) => ({type: CREATE_GUIDE, guide: values})
export const rateGuide = (id, note) => ({type: constants.RATE_GUIDE, guide_id: id, note: note})
export const getAllGuides = () => ({type: constants.GET_ALL_GUIDES})
export const getGuidePage = (id) => ({type: constants.GET_GUIDE_PAGE, id: id })
