import { GET_GENERAL_STATISTICS, GET_TOP_GUIDES, GET_LAST_GUIDES } from '../../constants/statistics/index'

export const getGeneralStats = () => ({type: GET_GENERAL_STATISTICS})
export const getLastGuides = () => ({type: GET_LAST_GUIDES})
export const getTopGuides = () => ({type: GET_TOP_GUIDES})