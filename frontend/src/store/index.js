import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware  from 'redux-saga'
import Reducers from '../reducers/index'
import Sagas from '../sagas/index'

const sagaMiddleware = createSagaMiddleware()

const reduxDevTools = window.devToolsExtension ? window.devToolsExtension() : f => f

const store = createStore(Reducers, compose(applyMiddleware(sagaMiddleware), reduxDevTools))

sagaMiddleware.run(Sagas);

export default store