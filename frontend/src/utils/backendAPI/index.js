import axios from 'axios'
const settings = require('../../settings.json')

export const postCallApi = (route, obj, isCredential) => {
    if (isCredential === true) {
        axios.defaults.withCredentials = true
    }
    return axios.post(settings.back_api + route, obj)
}

export const getCallApi = (route, isCredential) => {
    if (isCredential === true) {
        axios.defaults.withCredentials = true
    }
    return axios.get(settings.back_api + route)
}

export const deleteCallApi = (route, isCredential) => {
    if (isCredential === true) {
        axios.defaults.withCredentials = true
    }
    return axios.delete(settings.back_api + route)
}