import axios from 'axios'
const settings = require('../../settings.json')

export const getCallApi = (route) => {
    return axios.get(settings.dota_api + route)
}