import React from 'react'
import { List, Avatar } from 'antd'
import moment from 'moment'

class LastGuides extends React.Component {
    render() {
        return (
            <div>
                <List
                    dataSource={this.props.guides.filter(elem => elem.hero_id === this.props.hero_id)}
                    pagination={{
                        pageSize: 10
                    }}
                    renderItem={item => (
                        <List.Item>
                            <List.Item.Meta
                                avatar={<Avatar size="large" src={this.props.img}/>}
                                title={item.title}
                                description={moment(item.creation_date).format('MM/DD/YYYY')}
                            />
                            <div>
                                {item.content}
                            </div>
                        </List.Item>
                    )}
                />

            </div>
        )
    }
}

export default LastGuides