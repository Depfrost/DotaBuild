import React from 'react'
import { Table, Progress } from 'antd'

const columns = [
    {
        title: 'Name',
        dataIndex: 'name',
        width: 200
    }, {
        title: 'Games',
        dataIndex: 'games',
        width: 200,
        sorter: (a, b) => b.games - a.games
    }, {
        title: 'Wins Rate',
        dataIndex: 'percent',
        width: 300,
        render: (e) => <Progress percent={e} status="active" />,
        sorter: (a, b) => b.percent - a.percent
    }]

class MatchUps extends React.Component {
    render() {
        return (
            <div>
                <Table
                    loading={this.props.matchups.length === 0}
                    columns={columns}
                    dataSource={this.props.matchups.map(elem => {
                        const name = this.props.champions.find(champion => champion.id === elem.hero_id).localized_name
                        return {
                            key: elem.hero_id,
                            name: name,
                            games: elem.games_played,
                            percent: elem.percent
                        }
                    })} 
                />
            </div>
        )
    }
}

export default MatchUps