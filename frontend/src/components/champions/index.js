import LastMatchsTab from './lastmatch'
import MatchupsTab from './matchups'
import TopPlayerTab from './topplayer'
import LastGuidesTab from './lastguide'

export default {
    LastMatchsTab,
    MatchupsTab,
    TopPlayerTab,
    LastGuidesTab
}