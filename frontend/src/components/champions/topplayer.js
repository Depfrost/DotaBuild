import React from 'react'
import { List, Avatar } from 'antd'

class TopPlayer extends React.Component {
    render() {
        return (
            <div>
                <List
                    loading={this.props.players.length === 0}
                    dataSource={this.props.players}
                    renderItem={item => (
                        <List.Item>
                            <List.Item.Meta
                                avatar={<Avatar size="large" src={item.profile.avatar}/>}
                                title={item.profile.personaname}
                                description={item.wins + " Wins"}
                            />
                            {"MMR : " + item.mmr_estimate.estimate}
                        </List.Item>
                    )}
                />
            </div>
        )
    }
}

export default TopPlayer