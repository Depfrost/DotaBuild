import React from 'react'
import { List, Avatar } from 'antd'

class LastMatch extends React.Component {
    render() {
        return (
            <div>
                <List
                    loading={this.props.matchs.length === 0}
                    dataSource={this.props.matchs}
                    renderItem={item => (
                        <List.Item>
                            <List.Item.Meta
                                avatar={<Avatar size="large" src={item.profile.avatar}/>}
                                title={item.profile.personaname}
                                description={item.kills + '/' + item.deaths + '/' + item.assists}
                            />
                            <div>
                                {item.duration + ' min'}
                            </div>
                        </List.Item>
                    )}
                />
            </div>
        )
    }
}

export default LastMatch