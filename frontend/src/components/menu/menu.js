import React from 'react'
import { Menu, Layout } from 'antd';
import { Link } from 'react-router-dom'
import { logOut } from '../../actions/user/index'

class MenuNavigation extends React.Component {
    render() {
        /* CHANGE IF WE ARE CONNECTED */
        return (
            <div>
            <Layout.Header>
                <Menu mode="horizontal" theme="dark" style={{ lineHeight: '64px' }} selectedKeys={[]}>
                    <Menu.Item>
                        <Link to="/">Actualities</Link>
                    </Menu.Item>
                    <Menu.Item>
                        <Link to="/champions">Heroes</Link>
                    </Menu.Item>
                    <Menu.Item>
                        <Link to="/guides">Guides</Link>
                    </Menu.Item>
                    <Menu.Item>
                        <Link to="/items">Items</Link>
                    </Menu.Item>
                    <Menu.Item>
                        <Link to="/statistics">General Statistics</Link>
                    </Menu.Item>
                    {
                        this.props.user.isConnected === false
                        ? <Menu.Item style={{float: 'right'}}><Link to="/inscription">Register</Link></Menu.Item>
                        : <Menu.Item style={{float: 'right'}}><Link to="/" onClick={() => logOut()}>Log Out</Link></Menu.Item>
                    }
                    {
                        this.props.user.isConnected === false
                        ? <Menu.Item style={{float: 'right'}}><Link to="/connexion">Sign In</Link></Menu.Item>
                        : <Menu.Item style={{float: 'right'}}><Link to="/profil">Profile</Link></Menu.Item>
                    }
                </Menu>
            </Layout.Header>        
            </div>
        )
    }
}

export default MenuNavigation