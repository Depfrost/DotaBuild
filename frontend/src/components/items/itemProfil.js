import React from 'react'
import { Tree } from 'antd'

class ItemProfil extends React.Component {
    render() {
        return (
            <div>
                <p>Cost {this.props.item.cost}</p>
                <Tree>
                    <Tree.TreeNode title="tags in shop" defaultExpandAll={true}>
                    {
                        this.props.item.tags.map(elem => {
                            return (
                                <Tree.TreeNode key={elem} title={elem} />
                            )
                        })
                    }
                    </Tree.TreeNode>
                </Tree>
                {
                    this.props.item.isUpdate
                    ? <p>This object is an improvement of another</p>
                    : <p>This object is a basic object</p>
                }
            </div>
        )
    }
}

export default ItemProfil