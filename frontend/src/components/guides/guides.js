import React from 'react'
import { Table, Progress } from 'antd'

const columns = [
    {
        title: 'Title',
        dataIndex: 'title',
        width: 200
    }, {
        title: 'Votes',
        dataIndex: 'votes',
        width: 200
    }, {
        title: 'Score',
        dataIndex: 'percent',
        width: 300,
        render: (e) => <Progress percent={e} status="active" />
    }]

class GuidesList extends React.Component {
    render() {
        return (
            <div>
                <Table
                    columns={columns}
                    dataSource={this.props.guides.map(elem => {
                        const title = this.props.guides.find(guide => guide.id === elem.guides.id)
                        return {
                            key: elem.guides.id,
                            title: title,
                            votes: elem.votes,
                            percent: elem.percent
                        }
                    })}
                />
            </div>
        )
    }
}

export default GuidesList