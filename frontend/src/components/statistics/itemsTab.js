import React from 'react'
import { Table, Input, Row } from 'antd'

class ItemsTab extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            filter: ""
        }
        this.columns = [
            {
                title: "Name",
                dataIndex: "item",
                render: (record) => <p>{record.split("_").join(" ")}</p>
            }, {
                title: "Heroe",
                dataIndex: "heroe"
            }, {
                title: "Games",
                dataIndex: "games",
                sorter: (a, b) => b.games - a.games
            }, {
                title: "Win Rates",
                dataIndex: "percent",
                render: (record) => this.renderColumns(record),
                sorter: (a, b) => b.percent - a.percent
            }, {
                title: "Time",
                dataIndex: "time",
                render: (val) => <p>{val} min. </p>
            }
        ]
    }

    renderColumns = (val) => {
        if (val > 50) {
            return <font color="green">{val} %</font>
        }
        if (val < 50) {
            return <font color="red">{val} %</font>
        }
        return <font>{val} %</font>
    }

    render() {
        return (
            <div>
                <Row>
                    <Input placeholder="Look for a specific hero" onChange={(e) => this.setState({filter: e.target.value})}/>
                </Row>
                <br/>
                <Row>
                    <Table
                        columns={this.columns}
                        dataSource={this.props.stats.filter(elem => elem.heroe.toLowerCase().match(this.state.filter))}
                    />
                </Row>
            </div>
        )
    }
}

export default ItemsTab