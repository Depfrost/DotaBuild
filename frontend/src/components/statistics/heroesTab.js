import React from 'react'
import { Table, Row, Input } from 'antd'

class HeroesTab extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            filter: ""
        }
        this.columns = [
            {
                title: 'Name',
                dataIndex: 'name'
            }, {
                title: 'Number of Games',
                dataIndex: 'games',
                sorter: (b, a) => a.games - b.games
            }, {
                title: 'Wins Rate',
                dataIndex: 'percent',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent - b.percent
            }, {
                title: '1 pick',
                dataIndex: 'percent1',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent1 - b.percent1
            }, {
                title: '2 pick',
                dataIndex: 'percent2',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent2 - b.percent2
            }, {
                title: '3 pick',
                dataIndex: 'percent3',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent3 - b.percent3
            }, {
                title: '4 pick',
                dataIndex: 'percent4',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent4 - b.percent4
            }, {
                title: '5 pick',
                dataIndex: 'percent5',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent5 - b.percent5
            }, {
                title: '6 pick',
                dataIndex: 'percent6',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent6 - b.percent6
            }, {
                title: '7 pick',
                dataIndex: 'percent7',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent7 - b.percent7
            }, {
                title: '8 pick',
                dataIndex: 'percent8',
                render: (record) => this.renderColumns(record),
                sorter: (b, a) => a.percent8 - b.percent8
            }
        ]
    }

    renderColumns = (val) => {
        if (val > 50) {
            return <font color="green">{val} %</font>
        }
        if (val < 50) {
            return <font color="red">{val} %</font>
        }
        return <font>{val} %</font>
    }

    render() {
        return (
            <div>
                <Row>
                    <Input placeholder="Look for a specific hero" onChange={(e) => this.setState({filter: e.target.value})}/>
                </Row>
                <br/>
                <Row>
                    <Table
                        columns={this.columns}
                        dataSource={this.props.stats.filter(elem => elem.name.toLowerCase().match(this.state.filter))}
                    />
                </Row>
            </div>
        )
    }
}

export default HeroesTab