import React from 'react'

class ErrorPage extends React.Component {
    render() {
        return <h1>You are not authorized to access here, please log in</h1>
    }
}

export default ErrorPage