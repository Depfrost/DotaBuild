import { GET_HEROES_STATISTICS_SUCCESS, GET_ITEMS_STATISTICS_SUCCESS, GET_LAST_GUIDES_SUCCESS, GET_TOP_GUIDES_SUCCESS } from '../../constants/statistics/index'

const initialState = {
    heroes: [],
    items: [],
    topGuides: [],
    lastGuides: []
}

const statisticsReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_LAST_GUIDES_SUCCESS:
            return {
                ...state,
                lastGuides: [...action.guides]
            }
        case GET_TOP_GUIDES_SUCCESS:
            return {
                ...state,
                topGuides: [...action.guides]
            }
        case GET_HEROES_STATISTICS_SUCCESS:
            return {
                ...state,
                heroes: [...action.stats]
            }
        case GET_ITEMS_STATISTICS_SUCCESS:
            return {
                ...state,
                items: [...action.stats]
            }
        default:
            return state
    }
}

export default statisticsReducer