import { GET_ALL_GUIDES_SUCCESS } from '../../constants/guides/index'

const initialState = []

const guidesReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_ALL_GUIDES_SUCCESS:
            return [...action.guides]
        default:
            return state
    }
}

export default guidesReducer