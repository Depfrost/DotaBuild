import { GET_GUIDE_PAGE_SUCCESS } from '../../constants/guides/index'

const initialState = {}

const guideReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_GUIDE_PAGE_SUCCESS:
            return {
                ...state,
                ...action.guide
            }
        default:
            return state
    }
}

export default guideReducer