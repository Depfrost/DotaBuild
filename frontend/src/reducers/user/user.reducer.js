import { CONNECTION_USER_SUCCESS, LOGOUT_USER_SUCCESS, GET_USER_INFORMATION_SUCCESS, GET_USER_GUIDES_SUCCESS, DELETE_USER_GUIDE_SUCCESS } from '../../constants/user/index'

const initialState = {
    isConnected: false,
    username: "",
    email: "",
    guides: []
}

const userReducer = (state = initialState, action) => {
    switch(action.type) {
        case DELETE_USER_GUIDE_SUCCESS:
            const newArray = state.guides.filter(elem => elem._id !== action.id)
            return {
                ...state,
                guides: [...newArray]
            }
        case GET_USER_INFORMATION_SUCCESS:
            return {
                ...state,
                username: action.user.username,
                email: action.user.email
            }
        case GET_USER_GUIDES_SUCCESS:
            return {
                ...state,
                guides: [...action.guides]
            }
        case CONNECTION_USER_SUCCESS:
            return {
                ...state,
                isConnected: true
            }
        case LOGOUT_USER_SUCCESS:
            return {
                ...state,
                isConnected: false
            }
        default:
            return state
    }
}

export default userReducer