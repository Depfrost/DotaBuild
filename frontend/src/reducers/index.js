import { combineReducers } from 'redux'
/* FULL IMPORT REDUCERS */
import guidesReducer from './guides/guides.reducer'
import guideReducer from './guides/guide.reducer'
import championsReducer from './champions/champions.reducer'
import championProfil from './champions/profil.reducer'
import itemsReducer from './items/items.reducer'
import statisticsReducer from './statistics/statistics.reducer'
import itemProfilReducer from './items/profil.reducer'
import userReducer from './user/user.reducer'

export default combineReducers({
    /* NAMED REDUCER : OBJECTS REDUCERS */
    guide: guideReducer,
    guides: guidesReducer,
    champions: championsReducer,
    champion: championProfil,
    items: itemsReducer,
    stats: statisticsReducer,
    item: itemProfilReducer,
    user: userReducer
})