import { GET_ALL_HEROES_SUCCESS } from '../../constants/champions/index'

const initialState = [

]

const championsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_HEROES_SUCCESS:
            return [...action.champions]
        default:
            return state
    }
}

export default championsReducer