import { GET_HEROE_PROFIL_SUCCESS, RESET_HEROE_PROFIL } from '../../constants/champions/index'

const initialState = {
    id: 0,
    name: "",
    roles: [],
    localized_name: "",
    matchups: [],
    players: [],
    matchs: []
}

const profilReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_HEROE_PROFIL_SUCCESS:
            return {
                ...state,
                ...action.profil
            }
        case RESET_HEROE_PROFIL:
            return {
                ...initialState
            }
        default:
            return state
    }
}

export default profilReducer