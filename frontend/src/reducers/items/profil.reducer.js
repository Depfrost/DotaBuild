import { GET_ITEM_PROFIL_SUCCESS } from '../../constants/items/index'

const initialState = {

}

const itemProfilReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_ITEM_PROFIL_SUCCESS:
            return {
                ...action.obj
            }
        default:
            return state
    }
}

export default itemProfilReducer