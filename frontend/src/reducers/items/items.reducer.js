import { GET_ALL_ITEMS_SUCCESS } from '../../constants/items/index'

const initialState = [

]

const itemsReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_ALL_ITEMS_SUCCESS:
            return [...action.items]
        default:
            return state
    }
}

export default itemsReducer