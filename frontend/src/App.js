import React, { Component } from 'react';
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import Application from './containers/application'
import Store from './store/index'

class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <BrowserRouter>
          <Application />
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
