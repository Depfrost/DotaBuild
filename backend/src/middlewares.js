exports.requiresLogin = function(req, res, next) {
    if (req.session && req.session.userId) {
        return next();
    } else {
        let err = new Error('You must be logged in order to view this page.');
        err.status = 401;
        return next(err);
    }
}