let User = require('./schemas/user');
let Guide = require('./schemas/guide');

exports.create_user = function(req, res, next) {
    if (req.body.email &&
        req.body.username &&
        req.body.password) {

        let userData = {
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
        }

        //use schema.create to insert data into the db
        User.create(userData, function (err, user) {
            if (err) {
                return next(err)
            }
        });
        res.status(201).send({url: req.originalUrl + ' created'});
    }
    else
    {
        res.status(400).send({url: req.originalUrl + ' missing parameters'});
    }
};

exports.get_user = function(req, res) {
    User.findById(req.session.userId).then(function (user) {
        return res.send(user);
    });
};

exports.get_user_guides = function(req, res) {
    Guide.find({user_id: req.session.userId}).then(function (guides) {
        return res.send(guides)
    })
}

exports.delete_guide = function(req, res, next) {
    Guide.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.status(500).send({url: req.originalUrl + ' creation failed'});
            return next(err)
        }
        res.status(200).send({url: req.originalUrl + ' created'})
    })
}

exports.login = function(req, res, next) {
    if (req.body.username && req.body.password) {
        User.authenticate(req.body.username, req.body.password, function (error, user) {
            if (error || !user) {
                console.log("fuck");
                let err = new Error('Wrong username or password.');
                err.status = 401;
                return next(err);
            } else {
                req.session.userId = user._id;
                res.status(200).send({url: req.originalUrl + ' connected'});
            }
        });
    } else {
        let err = new Error('All fields required.');
        err.status = 400;
        return next(err);
    }
};

exports.logout = function(req, res, next) {
    if (req.session) {
        // delete session object
        req.session.destroy(function(err) {
            if(err) {
                res.status(400).send({url: req.originalUrl + ' error'});
                return next(err);
            }
        });
    }
    res.status(200).send({url: req.originalUrl + ' logged out'});
};

exports.get_top_guides = function(req, res) {
    Guide.find({}).sort({nb_notes:-1}).limit(10).then(function (guides) {
        res.send(guides);
    });
}

exports.create_guide = function(req, res, next) {
    if (req.body.title &&
        req.body.content &&
        req.body.hero_id && req.session) {

        let username ="";
        User.findById(req.session.userId).then(function (user) {
            username = user.username;
            let guideData = {
                title: req.body.title,
                content: req.body.content,
                user_id: req.session.userId,
                username: username,
                necessary: req.body.necessary,
                good: req.body.good,
                situational: req.body.situational,
                score: 5,
                nb_notes: 1,
                hero_id: req.body.hero_id,
                creation_date: new Date(),
            }

            // Use schema.create to insert data into the db
            Guide.create(guideData, function (err, guide) {
                if (err) {
                    res.status(500).send({url: req.originalUrl + ' creation failed'});
                    return next(err)
                }
                res.status(201).send({url: req.originalUrl + ' created'});
            });
        });
    }
    else
    {
        if (req.session)
            res.status(400).send({url: req.originalUrl + ' missing parameters'});
        else
            res.status(400).send({url: req.originalUrl + ' not logged'});
    }
};

exports.get_guides = function(req, res) {
    if (req.query.limit)
    {
        Guide.find({}).sort({creation_date:-1}).limit(parseInt(req.query.limit)).then(function (guides) {
            res.send(guides);
        });
    }
    else {
        Guide.find({}).then(function (guides) {
            res.send(guides);
        });
    }
};

exports.submit_note = function(req, res, next) {
    Guide.findById(req.body.guide_id, function (err, guide) {
        if (err) {
            res.status(500).send({url: req.originalUrl + ' notation failed'});
            return next(err);
        }
        guide.nb_notes = guide.nb_notes + 1;
        guide.score = (guide.score * (guide.nb_notes - 1) + parseFloat(req.body.note)) / guide.nb_notes;
        guide.save();
        res.status(200).send({url: req.originalUrl + ' score added'});
    });
};

exports.redirect = function(req, res) {
    res.sendFile('index.html', {root: __dirname + './../public'});
};