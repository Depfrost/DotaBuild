'use strict';
module.exports = function(serv) {
    const controller = require('./serverController');
    const middleware = require('./middlewares');

    serv.route('/user')
        .get(middleware.requiresLogin, controller.get_user)
        .post(controller.create_user);

    serv.route('/user/guides')
        .get(middleware.requiresLogin, controller.get_user_guides)

    serv.route('/login')
        .post(controller.login);

    serv.route('/logout')
        .get(controller.logout);

    serv.route('/guides')
        .get(controller.get_guides)
        .post(middleware.requiresLogin, controller.create_guide)
        
    serv.route('/guide/:id')
        .delete(middleware.requiresLogin, controller.delete_guide)

    serv.route('/notes')
        .post(middleware.requiresLogin, controller.submit_note);

    serv.route('/')
        .get(controller.redirect);

    serv.route('/guides/top')
        .get(controller.get_top_guides)

};
