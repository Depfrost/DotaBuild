var mongoose = require('mongoose');

let GuideSchema = new mongoose.Schema({
    title: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    user_id: {
        type: String,
        required: true
    },
    username: {
        type: String
    },
    content: {
        type: String
    },
    necessary: [
        String
    ],
    good: [
        String
    ],
    situational: [
        String
    ],
    score: {
        type: mongoose.Schema.Types.Decimal128
    },
    nb_notes: {
        type: Number
    },
    hero_id: {
        type: Number
    },
    creation_date: {
        type: Date
    }
});

let Guide = mongoose.model('Guide', GuideSchema);
module.exports = Guide;