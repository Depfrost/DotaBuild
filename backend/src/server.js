const express = require('express'),
    serv = express(),
    port = 4242;

let mongoose = require('mongoose');
let session = require('express-session');
let MongoStore = require('connect-mongo')(session);


mongoose.connection.close();
mongoose.connect('mongodb+srv://server:serverpass@cluster0-lypkl.mongodb.net/test');
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
    console.log("opened");
});
serv.set('trust proxy', 1);
//use sessions for tracking logins
serv.use(session({
    secret: 'work',
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({
        mongooseConnection: db
    })
}));

const cors = require('cors');
serv.use(cors({
    origin:['http://localhost:3000'],
    methods:['GET','POST', 'PUT', 'DELETE'],
    // Enable storing session cookie
    credentials: true
}));

const minify = require('express-minify');
serv.use(minify());
serv.use(express.static('./public'));

const bodyParser = require('body-parser');
serv.use(bodyParser());

const routes = require('./serverRoutes');
routes(serv);
serv.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

serv.listen(port);

console.log('API server started on: ' + port);